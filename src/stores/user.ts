import { defineStore } from "pinia";
import {ref} from "vue";
import type User from "@/types/User";
import UserService from "../services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("user", () => {
  const user = ref<User[]>([]);
  const loading = useLoadingStore();
  const messageStore = useMessageStore();
  const editedUser = ref<User>({ login: "", name: "", password: "" });
  const dialog = ref(false);

  async function getUser() {
    loading.isLoading = true;
    const res = await UserService.getUser();
    user.value = res.data;
    loading.isLoading = false;
  }

  async function saveUser() {
    try {
      loading.isLoading = true;
      if (editedUser.value.id) {
        await UserService.updateUser(editedUser.value.id, editedUser.value);
      } else {
        await UserService.saveUser(editedUser.value);
      }
    } catch (e) {
      console.log(e);
    }
    await getUser();
    loading.isLoading = false;
    dialog.value = false;
  }

  function editUser(User: User) {
    editedUser.value = JSON.parse(JSON.stringify(User));
    dialog.value = true;
  }
  async function deleteUser(id: number) {
    loading.isLoading = true;
    try {
      const res = await UserService.deleteUser(id);
      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't Delete User");
    }
    loading.isLoading = false;
  }


  return { user, getUser, saveUser, dialog, editedUser, editUser, deleteUser };
});

